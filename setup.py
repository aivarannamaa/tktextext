from setuptools import setup

setup(
      name="tktextext",
      version=1.0,
      description="Extensions for tkinter.Text (provides read-only, scrollbars, line numbers etc)",
      url="https://bitbucket.org/aivarannamaa/tktextext",
      license="MIT",
      classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "License :: Freeware",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "Topic :: Software Development",
        "Topic :: Software Development :: Libraries",
      ],
      keywords="tk tkinter Text",
      py_modules=["tktextext"],
)
